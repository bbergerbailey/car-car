import React, { useState, useEffect } from 'react';


function ServiceAppointmentForm() {
    const [technicians, setTechnicians] = useState([])
    const [submitted, setSubmitted] = useState(false)
    const [formData, setFormData] = useState({
        vin: '',
        customer: '',
        date: '',
        time: '',
        technician: '',
        reason: '',
    })

    const getData = async () => {
        const url = 'http://localhost:8080/api/technicians/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians)
        }
    }

    useEffect(() => {
        getData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const locationUrl = 'http://localhost:8080/api/appointments/';

        const updatedFormData = {
            vin: formData.vin,
            customer: formData.customer,
            date_time: `${formData.date} ${formData.time}`,
            technician: formData.technician,
            reason: formData.reason,
        }

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(updatedFormData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(locationUrl, fetchConfig);

        if (response.ok) {
            setSubmitted(true)
            setFormData({
                vin: '',
                customer: '',
                date: '',
                time: '',
                technician: '',
                reason: '',
            });
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value,
        });
    }

    const showMessage = submitted ? 'alert alert-success mt-4 offset-3 col-6' : 'alert alert-success d-none mb-0 offset-3 col-6';

    return (
        <>
            <div className={showMessage} id="form-success">
                Successfully Scheduled a Service Appointment
            </div>
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a New Service Appointment</h1>
                        <form onSubmit={handleSubmit} id="create-service-appointment-form">
                            <div className="form-floating mb-3">
                                <input onChange={handleFormChange} value={formData.vin} placeholder="Automobile VIN" required type="text" name="vin" id="vin" className="form-control" />
                                <label htmlFor="vin">Automobile VIN</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleFormChange} value={formData.customer} placeholder="Customer" required type="text" name="customer" id="customer" className="form-control" />
                                <label htmlFor="customer">Customer</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleFormChange}  value={formData.date} placeholder="Date" required type="date" name="date" id="date" className="form-control" />
                                <label htmlFor="date">Date</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleFormChange}  value={formData.time} placeholder="Time" required type="time" name="time" id="time" className="form-control" />
                                <label htmlFor="time">Time</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={handleFormChange} value={formData.technician} required name="technician" id="technician" className="form-select">
                                    <option value="">Choose a Technician</option>
                                    {technicians.map(technician => {
                                        return (
                                            <option key={technician.id} value={technician.id}>{technician.first_name} {technician.last_name}</option>
                                        )
                                    })}
                                </select>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleFormChange}  value={formData.reason} placeholder="Reason" required type="text" name="reason" id="reason" className="form-control" />
                                <label htmlFor="reason">Reason</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </>
    )
}

export default ServiceAppointmentForm;
