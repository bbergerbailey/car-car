import { useEffect, useState } from 'react'


function SalespersonHistory() {
    const [salespeople, setSalespeople] = useState([])
    const [salesperson, setSalesperson] = useState('')
    const [salespersonSales, setSalespersonSales] = useState([])

    const getSalesPeopleData = async () => {
        const response = await fetch('http://localhost:8090/api/salespeople/');

        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespeople)
        }
    }

    useEffect(()=>{
        getSalesPeopleData()
      }, []);

    const handleSalespersonSelect = async (e) => {
        const href = e.target.value
        setSalesperson(href)
        const response = await fetch(`http://localhost:8090/api/sales/`)
        if (response.ok) {
            const data = await response.json();
            const sales = []
            for (let sale of data.sales) {
                if (sale.salesperson.href === href) {
                    sales.push(sale)
                }

            }
            setSalespersonSales(sales)
        }

    }

    const hideTable = salesperson ?  'table table-striped' : 'table table-striped d-none'

    return (
    <>
        <h1 className="mt-4 mb-4">Salesperson History</h1>
        <form>
            <div className="mb-3">
                <select onChange={handleSalespersonSelect} value={salesperson} required name="salesperson" id="salesperson" className="form-select">
                    <option value="">Select a Salesperson</option>
                    {salespeople.map(salesperson => {
                    return (
                    <option key={salesperson.id} value={salesperson.href}>{salesperson.first_name} {salesperson.last_name}</option>
                    )
                    })}
                </select>
            </div>
        </form>
        <table className={hideTable}>
                <thead>
                    <tr>
                        <th>Salesperson Name</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {salespersonSales.map(sale => {
                        return (
                        <tr key={sale.id}>
                            <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                            <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                            <td>{sale.automobile.vin}</td>
                            <td>{sale.price}</td>
                        </tr>
                        );
                    })}
                </tbody>
            </table>
    </>
        );
    };

export default SalespersonHistory;
