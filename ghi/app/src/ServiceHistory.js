import { useState } from 'react';

function ServiceHistory() {
    const [appointments, setAppointments ] = useState([])
    const [vin, setVin] = useState('')
    const [submit, setSubmit] = useState(false)
    const [hasHistory, setHasHistory] = useState(true)

    const handleSubmit = async (event) => {
        event.preventDefault();

        const response = await fetch('http://localhost:8080/api/appointments/');

        if (response.ok) {
            const data = await response.json();

            const serviceHistory = []
            for (let appointment of data.appointments) {
                if (appointment.vin === vin) {
                    serviceHistory.push(appointment)
                    setSubmit(true)
                    setHasHistory(true)
                    setVin('')
                } else {
                    setSubmit(false)
                    setHasHistory(false)

                }
            }
            setAppointments(serviceHistory)
        }
    }

    const handleFormChange = (e) => {
        const vin = e.target.value;
        setVin(vin)
    }

    const showMessage = hasHistory ? 'd-none' : 'alert alert-danger mb-0'
    const hideTable = submit ?  'table table-striped' : 'table table-striped d-none'

    return (
        <>
            <h1 className="mt-4 mb-4">Service History</h1>
            <div className={showMessage} id="invalid-vin">
                VIN# not in Service History database
            </div>
            <form onSubmit={handleSubmit} className="d-flex mt-4 mb-4">
                <input onChange={handleFormChange} value={vin} className="form-control me-2" type="search" name="vin" id="vin" placeholder="Search VIN#" aria-label="Search" />
                <button className="btn btn-primary" type="submit">Search</button>
            </form>
            <table className={hideTable}>
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Is VIP?</th>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.map(appointment => {
                    return (
                        <tr key={appointment.id}>
                            <td>{appointment.vin}</td>
                            {appointment.is_vip ? <td>Yes</td> : <td>No</td>}
                            <td>{appointment.customer}</td>
                            <td>{new Date(appointment.date_time).toLocaleDateString()}</td>
                            <td>{new Date(appointment.date_time).toLocaleTimeString()}</td>
                            <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                            <td>{appointment.reason}</td>
                            <td>{appointment.status}</td>
                        </tr>
                    );
                })}
                </tbody>
            </table>
        </>
    )
}

export default ServiceHistory;
